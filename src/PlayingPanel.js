import React from 'react'
import './playingPanel.less'



class PlayingPanel extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      userAnswer: null,
      right:null
    };
    this.handleUserNumber = this.handleUserNumber.bind(this);
    this.handleIsRight = this.handleIsRight.bind(this);
  }

  handleUserNumber(event){
    const v1 = event.target.value;
    this.setState({
      userAnswer:v1
    },()=>{
      console.log(this.state.userAnswer);
      document.getElementById('user1').value = (this.state.userAnswer)[0];
      document.getElementById('user2').value = (this.state.userAnswer)[1]===undefined ? null:(this.state.userAnswer)[1];
      document.getElementById('user3').value = (this.state.userAnswer)[2]===undefined ? null:(this.state.userAnswer)[2];
      document.getElementById('user4').value = (this.state.userAnswer)[3]===undefined ? null:(this.state.userAnswer)[3];
    });
  }

  handleIsRight(){
    this.setState({
      right:'true'
    },()=>{
      console.log("999999999999");
    })
  }

  render() {
    return (
      <div>
        <div>
          <label>Your Result</label>

          <div>
            <input type='text' className='num' id='user1'/>
            <input type='text' className='num' id = 'user2'/>
            <input type='text' className='num' id='user3'/>
            <input type='text' className='num' id = 'user4'/>
          </div>

          <label>{this.state.right}</label>

        </div>

        <div className='guessCard'>
          <label>Guess Card</label>
          <div>
            <input type='text' maxLength='4' onChange={this.handleUserNumber}/>
          </div>
          <input type='button' value='guess' onClick={this.handleIsRight}/>
        </div>

      </div>
    );
  }

}

export default PlayingPanel;