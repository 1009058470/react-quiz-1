import React from 'react'
import PlayingPanel from "./PlayingPanel";
import ControlPanel from "./ControlPanel";
import './common.less'


class App extends React.Component {
  constructor(props){
    super(props);
  }



  render() {
    return (
      <div>
        <div className='playingPanel'>
          <PlayingPanel />
        </div>

        <div className='controlPanel'>
          <ControlPanel />
        </div>

    </div>);
  }

}

export default App;