import React from 'react'

class ControlPanel extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      answer:0,
      justResult:0
    };
    this.newGame = this.newGame.bind(this);
    this.showResult = this.showResult.bind(this);
  }



  getRanNum(result) {
    for (var i = 0; i < 4; i++) {
      var ranNum = Math.ceil(Math.random() * 25); //生成一个0到25的数字
      result.push(String.fromCharCode(65 + ranNum));
    }
  }


  newGame(){
    let ans = [];
    this.getRanNum(ans);
    console.log(ans[0],ans[1],ans[2],ans[3]);
    this.setState({
      answer:ans
    },()=>{
      document.getElementById('ans1').value = (this.state.answer)[0];
      document.getElementById('ans2').value = (this.state.answer)[1];
      document.getElementById('ans3').value = (this.state.answer)[2];
      document.getElementById('ans4').value = (this.state.answer)[3];

      setTimeout(() =>{
        document.getElementById('ans1').value = null;
        document.getElementById('ans2').value = null;
        document.getElementById('ans3').value = null;
        document.getElementById('ans4').value = null;
      },3000)

    });
  }

  showResult(){  // 没有清空左边的内容
    this.setState({
      justResult: this.state.justResult+1
    },() =>{
      document.getElementById('ans1').value = (this.state.answer)[0];
      document.getElementById('ans2').value = (this.state.answer)[1];
      document.getElementById('ans3').value = (this.state.answer)[2];
      document.getElementById('ans4').value = (this.state.answer)[3];
    });
  }


  render() {
    return (
      <div>

        <div>
          <label>New Card</label>
        </div>

        <input type='button' value='New Card' onClick={this.newGame}/>
        <div>
          <input type='text' id='ans1'/>
          <input type='text' id='ans2'/>
          <input type='text' id='ans3'/>
          <input type='text' id='ans4'/>
        </div>
        <input type='button' value='Show Result' onClick={this.showResult}/>
      </div>
    )
  }

}

export default ControlPanel;